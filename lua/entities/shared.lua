if SERVER then
	AddCSLuaFile()

	resource.AddFile("materials/vgui/ttt/icon_jack.vmt")
	resource.AddFile("materials/vgui/ttt/sprite_jack.vmt")
end

local roleName = "TIM"

-- creates global var "ROLE_TIM" and other required things
-- TEAM_[name], data: e.g. icon, color, ...
InitCustomTeam(roleName, {
		icon = "vgui/ttt/sprite_jack",
		color = Color(59, 215, 222, 255)
})

-- important to add roles with this function,
-- because it does more than just access the array ! e.g. updating other arrays
InitCustomRole(roleName, 
	{ -- first param is access for ROLES array => ROLES["TIM"] or ROLES["JESTER"]
		color = Color(59, 215, 222, 255), -- ...
		dkcolor = Color(1, 184, 193, 255), -- ...
		bgcolor = Color(255, 154, 67, 255), -- ...
		abbr = "pstl", -- abbreviation
		defaultTeam = ROLE_TIM, -- the team name: roles with same team name are working together
		defaultEquipment = INNO_EQUIPMENT, -- here you can set up your own default equipment
		surviveBonus = 0, -- bonus multiplier for every survive while another player was killed
		scoreKillsMultiplier = 0, -- multiplier for kill of player of another team
		scoreTeamKillsMultiplier = 0, -- multiplier for teamkill
        traitorCreditAward = false, -- will receive credits on kill like a traitor
        notSelectable = true
	},	{

	}
)


hook.Add("TTT2FinishedLoading", "TESTInitT", function()

	if CLIENT then
		-- setup here is not necessary but if you want to access the role data, you need to start here
		-- setup basic translation !
		LANG.AddToLanguage("English", TIM.name, "TIM")
		LANG.AddToLanguage("English", ROLE_TIM, "ROLE TIM")
		LANG.AddToLanguage("English", "hilite_win_" .. ROLE_TIM, "THE TIM WON") -- name of base role of a team -> maybe access with GetBaseRole(ROLE_JESTER) or JESTER.baserole
		LANG.AddToLanguage("English", "win_" .. ROLE_TIM, "The TIM has won!") -- teamname
		LANG.AddToLanguage("English", "info_popup_" .. TIM.name, [[You are the TIM! Try to survive! Don't get killed by others and especially not by JAN]])
		LANG.AddToLanguage("English", "body_found_" .. TIM.abbr, "This was a TIM...")
		LANG.AddToLanguage("English", "search_role_" .. TIM.abbr, "This person was a TIM!")
		LANG.AddToLanguage("English", "ev_win_" .. ROLE_TIM, "The TIM won the round!")
		LANG.AddToLanguage("English", "target_" .. TIM.name, "TIM")
		LANG.AddToLanguage("English", "ttt2_desc_" .. TIM.name, [[Lorem ipsum]])
		---------------------------------

		-- maybe this language as well...
		LANG.AddToLanguage("Deutsch", TIM.name, "TIM")
		LANG.AddToLanguage("Deutsch", ROLE_TIM, "TEAM Test")
		LANG.AddToLanguage("Deutsch", "hilite_win_" .. ROLE_TIM, "THE TIM WON")
		LANG.AddToLanguage("Deutsch", "win_" .. ROLE_TIM, "Der TIM hat gewonnen!")
		LANG.AddToLanguage("Deutsch", "info_popup_" .. TIM.name, [[Du bist DER TIM! Versuche zu überleben! Lass dich nicht töten von anderen und vor allem nicht von JAN]])
		LANG.AddToLanguage("Deutsch", "body_found_" .. TIM.abbr, "Er war der TIM...")
		LANG.AddToLanguage("Deutsch", "search_role_" .. TIM.abbr, "Diese Person war der TIM!")
		LANG.AddToLanguage("Deutsch", "ev_win_" .. ROLE_TIM, "Der TIM hat die Runde gewonnen!")
		LANG.AddToLanguage("Deutsch", "target_" .. TIM.name, "TIM")
		LANG.AddToLanguage("Deutsch", "ttt2_desc_" .. TIM.name, [[Lorem ipsum]])
	end
end)